

## About This Project

This test project is done in Laravel. As mentioned in the test requirement, all the functionalities done. Line total,Sub total coupon apply calculations are done using jQuery and server side validations are not done.  Also default user authentication and routing are done.


## Installation

Download the git repo to local machine. 
Run composer Update. 
Import the db file 'fingent_test_db.sql' in the root folder, to the local machine since no migration are written.
Set env configurations
Then run project


## Testing

## Login credentials 
username : nidhin@cc.com
password : nidhin12

## Coupon Test
fixed value coupon : FIXED20
percentage value coupon : 10CENT
