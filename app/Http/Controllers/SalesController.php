<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Taxes;
use App\Models\Coupons;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo 'dsd';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function new_sale()
    {
        $taxes      = Taxes::all();
        $products   = Products::all();
        return view('sales.new',['products'=>$products,'taxes'=>$taxes]);
    }

    public function couponapply(Request $request)
    {
        $coupon     = $request->coupon;
        $couponData = Coupons::where('coupon_code','like',$coupon)->where('coupon_count', '!=',0)->first();
        if($couponData){
            echo json_encode($couponData);
        }else{
            echo 'nodata';
        }
    }
}
