<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    use HasFactory;
     protected $fillable = [
					        'coupon_type',
					        'coupon_code',
					        'coupon_value',
							'coupon_count'
						];
}
