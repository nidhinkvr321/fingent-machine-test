<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
     protected $fillable = [
					        'name',
					        'per_unit_price',
					        'in_stock',							
						];
}
