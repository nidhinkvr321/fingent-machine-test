@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">New Invoice <span id="add_new_product" data-rowvalue="2" style="float: right;">+Add new product</span></div>
                    <div class="card-body" id="printable">                    
                        <div class="table-responsive">
                            <form >
                            <table class="table table-striped table-vcenter table-hover mb-0"  id="table_pdct">
                                <thead>
                                    <tr> 
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Per Unit Price ($)</th>                                        
                                        <th>Count</th>
                                        <th>Tax</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>                
                                    @php $i= 1;@endphp       
                                    <tr id="row{{$i}}">
                                        <td style="text-align: center;">{{$i}}</td>
                                        <td>
                                            <select name="buyer[]" data-plugin-selectTwo class="form-control populate" onchange="getProductDetailsByID(this.value,{{$i}})">
                                                <option value="">Select Product</option>
                                                @foreach($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="text" name="per_unit_price[]" id="per_unit_price{{$i}}" class="form-control" readonly ></td> 
                                        <td><input type="number" name="quantity" id="quantity{{$i}}" min="1" max="5" class="form-control" value="1" onchange="clculateLineTotal({{$i}})"><br><br></td>
                                        <td>
                                            <select name="tax[]" data-plugin-selectTwo class="form-control populate" onchange="clculateLineTotal({{$i}})" id="tax{{$i}}">
                                                <option value="">Select Tax</option>
                                                @foreach($taxes as $tax)
                                                    <option value="{{ $tax->tax_value }}">{{ $tax->tax_value}}%</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="text-align: center;">
                                            <input type="text" name="linetotal[]" id="linetotal{{$i}}" class="form-control" readonly >
                                            <input type="hidden" name="linetotalwotax[]" id="linetotalwotax{{$i}}" class="form-control">
                                        </td>
                                    </tr>    
                                </tbody> 
                                <tfoot>
                                    <tr> 
                                        <th colspan="5">Sub Total (Without Tax)</th>
                                        <th><input type="text" name="subtotal" id="subtotalwotax" readonly></th>
                                    </tr>
                                    <tr> 
                                        <th colspan="5">Sub Total (With Tax)</th>
                                        <th>
                                            <input type="text" name="subtotaltax" id="subtotaltax" readonly>
                                            <input type="hidden" name="subtotalcoupontax" id="subtotalcoupontax" readonly>
                                        </th>
                                    </tr>
                                    <tr> 
                                        <th colspan="5">Apply Coupon</th>
                                        <th><input type="text" name="coupon" id="coupon">
                                            <a onclick="applyCoupon()">Apply</a>
                                        </th>
                                    </tr>
                                    <tr> 
                                        <th colspan="5"> </th>
                                        <th><input type="button" onclick="printInvoice('printable')" value="Print Invoice" /></th>
                                    </tr>                                  

                                </tfoot>
                            </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function getProductDetailsByID(product_id,line) {
            var CSRF_TOKEN =$("input[name=_token]").val();
            $.ajax({
                url: '/product/getbyid',
                type: 'POST',
                data: {_token: CSRF_TOKEN, id:product_id},
                dataType: 'json',
                success: function (data) {
                    var per_unit_price = data['per_unit_price'];
                    var quantity = data['in_stock'];
                    if(data['per_unit_price']!='')
                    {
                        $('#per_unit_price'+line).val(per_unit_price);
                        $('#quantity'+line).prop('max',quantity);
                    }
                    clculateLineTotal(line);                   

                }     
            });
                 
        }

        function clculateLineTotal(line) { 
            var per_unit_price = $('#per_unit_price'+line).val();
            var quantity = $('#quantity'+line).val();
            var tax = $('#tax'+line).val();

            if(per_unit_price!=0 && tax!=''){
                totalPricewoTax = per_unit_price*quantity;
                $('#linetotalwotax'+line).val(totalPricewoTax);
                if(tax==0){
                    var totalPrice = totalPricewoTax;
                    
                }else{
                    totalPrice = (totalPricewoTax) + (per_unit_price*quantity)*(tax/100);
                }
                $('#linetotal'+line).val(totalPrice);
            }
            sumLineTotals();
        }

        function sumLineTotals(){
            var table_row = $('#table_pdct >tbody >tr').length;
            var subTotalWithTax = 0;
            var subTotalWithOutTax = 0;
            for(i=1;i<=table_row;i++){
                subTotalWithTax+=parseFloat($('#linetotal'+i).val());
                subTotalWithOutTax+=parseFloat($('#linetotalwotax'+i).val());
            }
            if(!isNaN(subTotalWithOutTax))
                $('#subtotalwotax').val(subTotalWithOutTax);
            if(!isNaN(subTotalWithTax)){
                $('#subtotaltax').val(subTotalWithTax);
                $('#subtotalcoupontax').val(subTotalWithTax);
            }
                
        }

        $('#add_new_product').click(function(e) { 
            addNewProdctRow(
                $('#table_pdct >tbody >tr').length
            );
        });

        function addNewProdctRow(rownum){ 
            var tabledata = '<tr id="'+rownum+++'">'
                            +'<td style="text-align: center;">'+rownum+'</td>'
                            +'<td>'
                                +'<select name="buyer[]" data-plugin-selectTwo class="form-control populate" onchange="getProductDetailsByID(this.value,'+rownum+')">'
                                    +'<option value="">Select Product</option>'
                                    +'@foreach($products as $product)'
                                        +'<option value="{{ $product->id }}">{{ $product->name}}</option>'
                                    +'@endforeach'
                                +'</select>'
                            +'</td>'
                            +'<td><input type="text" name="per_unit_price[]" id="per_unit_price'+rownum+'" class="form-control" readonly ></td>'
                            +'<td><input type="number" name="quantity" id="quantity'+rownum+'" min="1" max="5" class="form-control" value="1" onchange="clculateLineTotal('+rownum+')"><br><br></td>'
                            +'<td>'
                                +'<select name="tax[]" data-plugin-selectTwo class="form-control populate" onchange="clculateLineTotal('+rownum+')" id="tax'+rownum+'">'
                                    +'<option value="">Select Tax</option>'
                                    +'@foreach($taxes as $tax)'
                                        +'<option value="{{ $tax->tax_value }}">{{ $tax->tax_value}}%</option>'
                                    +'@endforeach'
                                +'</select>'
                            +'</td>'
                            +'<td style="text-align: center;"><input type="text" name="linetotal[]" id="linetotal'+rownum+'" class="form-control" readonly > <input type="hidden" name="linetotalwotax[]" id="linetotalwotax'+rownum+'" class="form-control"></td>'
                            +'</tr>';
            $('#table_pdct').append(tabledata);
        }

        function applyCoupon() { debugger;
            var CSRF_TOKEN =$("input[name=_token]").val();
            var coupon = $('#coupon').val();
            $.ajax({
                url: 'couponapply',
                type: 'POST',
                data: {_token: CSRF_TOKEN, coupon:coupon},
                dataType: 'json',
                success: function (data) {
                    if(data=='nodata'){
                        alert("No coupon available");
                    }else{
                        coupon_type =data['coupon_type'];
                        coupon_value = data['coupon_value'];
                        var subtotaltax = $('#subtotalcoupontax').val();
                        if(coupon_type =='fixed'){
                            appliedValue = subtotaltax-coupon_value;
                        }else{
                            appliedValue = subtotaltax - (subtotaltax*(coupon_value/100));
                        }
                        $('#subtotaltax').val(appliedValue);
                        alert('Coupon Applied');
                    }
                    // var per_unit_price = data['per_unit_price'];
                    // var quantity = data['in_stock'];
                    // if(data['per_unit_price']!='')
                    // {
                    //     $('#per_unit_price'+line).val(per_unit_price);
                    //     $('#quantity'+line).prop('max',quantity);
                    // }
                    //clculateLineTotal(line);                   

                }     
            });
                 
        }

        function printInvoice(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>   
@endsection