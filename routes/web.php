<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(array('middleware'=>'auth'),function()
{
	Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
	Route::get('/sales', [SalesController::class, 'index'])->name('index');
	Route::get('/sales/newsale', [SalesController::class, 'new_sale'])->name('new_sale');
    Route::post('/sales/couponapply', [SalesController::class, 'couponapply'])->name('couponapply');	

    
	Route::post('/product/getbyid', [ProductController::class, 'getbyid'])->name('getbyid');
});